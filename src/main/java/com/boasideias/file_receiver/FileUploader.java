package com.boasideias.file_receiver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@RestController
@Slf4j
public class FileUploader {

    @PostMapping("")
    public void upload(@RequestHeader(required = false, name =  "X-Filename") String fileName,
                       @RequestBody MultipartFile file) throws IOException {

        var name = Optional.ofNullable(fileName).orElse(file.getOriginalFilename());
        var path = Path.of(System.getProperty("java.io.tmpdir"), name);
        Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

    }

}
