package com.boasideias.file_receiver;

import com.ngrok.HttpBuilder;
import com.ngrok.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.System.getenv;

@Component
@Slf4j
public class NgrokInitializer implements ApplicationRunner {

    private final Environment env;

    public NgrokInitializer(Environment env) {
        this.env = env;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        var builder = Session
                .withAuthtoken(Optional.ofNullable(getenv("NGROK_AUTHTOKEN")).orElseGet(this::tokenFromFile))
                .metadata("file uploader session");

        var session = builder.connect();

        var localAddress = "http://127.0.0.1:" + env.getProperty("local.server.port");
        var endpoint = new HttpBuilder(session).forward(new URI(localAddress).toURL());
        log.info("Endpoint: {}", endpoint.getUrl());

    }

    private String tokenFromFile() {

        return homeUser()
                .filter(path -> path.endsWith("ngrok.yml"))
                .peek(path -> log.info("Arquivo de configuração encontrado em: {}", path.getParent()))
                .findFirst()
                .map(this::readLines)
                .filter(line -> line.startsWith("authtoken"))
                .map(line -> line.replace("authtoken: ", "").trim())
                .orElseThrow(() -> new RuntimeException("Nenhum token encontrado. " +
                        "Faça login em https://dashboard.ngrok.com/get-started " +
                        "e siga os passos para persistência do token"));
    }

    private static Stream<Path> homeUser() {
        try {
            return Files.walk(Path.of(System.getProperty("user.home")));
        } catch (IOException e) {
            throw new RuntimeException("Erro ao recuperar o diretório padrão do usuário", e);
        }
    }

    private String readLines(Path path) {
        try {
            return Files.readAllLines(path).get(1);
        } catch (IOException e) {
            throw new RuntimeException("Erro ao ler o arquivo de configuração", e);
        }
    }
}
